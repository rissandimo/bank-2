import firebase from 'firebase/app';
import 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyA-YH-zNv81ZpykY_qE38Fk9jzRUpHdEXw",
    authDomain: "bank-2-d5908.firebaseapp.com",
    databaseURL: "https://bank-2-d5908.firebaseio.com",
    projectId: "bank-2-d5908",
    storageBucket: "bank-2-d5908.appspot.com",
    messagingSenderId: "753100130560",
    appId: "1:753100130560:web:f861974a82e311f0af4be5"
  };

  firebase.initializeApp(firebaseConfig);

  export const auth = firebase.auth();