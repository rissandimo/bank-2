import React from 'react';
import './Login.css'
import { Button, FormControl, TextField } from '@material-ui/core';

class Login extends React.Component{

    constructor(){
        super();

        this.state = {
            email: '',
            password: ''
        }
    }

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    }

    handleSubmit = event => {
        event.preventDefault();
    }
    
    render(){
        return(
            <div className="login">
                <form onClick={this.handleSubmit} className='login__form'>
                <FormControl>
                    <TextField label="Email" name='email' value={this.state.email} onChange={this.handleChange} />
                    <TextField label="Password" name='password' value={this.state.password} onChange={this.handleChange} />
                </FormControl>
                    <Button variant="contained" color="primary" type="submit" className='login__button'>Login</Button>
                </form>
            </div>
        )
    }
}

export default Login;