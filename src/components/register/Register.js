import React from 'react';
import './Register.css'

import { auth } from '../../firebase/firebase';


class Register extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            // displayName: '',
            email: '',
            password: '',
            confirmPassword: ''
        }

    }

    handleInputChange = (event) => {

       this.setState({
           [event.target.name]: event.target.value
       })
    }

    handleSubmit = event => {
        event.preventDefault();
        
        const { email, password, confirmPassword} = this.state;

        if(password !== confirmPassword){
            alert("Passwords don't match");
            return;
        }

        auth.createUserWithEmailAndPassword(email, password)
        .catch(function(error) {
            console.log('Error creating user - ', error.message);
        })

        this.setState({
            email: '',
            password: '',
            confirmPassword: ''
        });
    }

  render () {
      const {email, displayName, password, confirmPassword} = this.state
    return (
      <div>
        <form className='register__form' onSubmit={this.handleSubmit}>
            {/* <input value={displayName} placeholder='Full Name' name='displayName' onChange={this.handleInputChange}  /> */}
            <input value={email} placeholder='email' name='email'  onChange={this.handleInputChange}/>
            <input value={password} type='password' placeholder='password' name='password'  onChange={this.handleInputChange}/>
            <input value={confirmPassword} type='password' placeholder='confirm password' name='confirmPassword' onChange={this.handleInputChange}/>
          <button onSubmit={this.handleSubmit}>Register</button>
        </form>
      </div>
    )
  }
}

export default Register;
